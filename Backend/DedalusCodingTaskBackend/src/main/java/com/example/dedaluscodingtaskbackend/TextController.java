package com.example.dedaluscodingtaskbackend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/api/text")
public class TextController {

  @Autowired
  private TextService textService;

  @PostMapping("/analyze")
  public Map<String, Integer> analyzeTextCall(@RequestBody TextData data) {
    String text = data.getText();
    boolean isVowel = data.getIfVowel();
    return textService.analyzeText(text, isVowel);
  }
}
