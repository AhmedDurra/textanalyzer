package com.example.dedaluscodingtaskbackend;

public class TextData
{
  private final String text;
  private final boolean isVowel;

  public TextData(boolean isVowel, String text) {
    this.text = text;
    this.isVowel = isVowel;
  }

  public boolean getIfVowel() {
    return isVowel;
  }

  public String getText() {
    return text;
  }
}
