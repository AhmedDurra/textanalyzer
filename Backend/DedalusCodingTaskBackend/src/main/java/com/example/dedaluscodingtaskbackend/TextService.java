package com.example.dedaluscodingtaskbackend;

import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TextService {
  public Map<String, Integer> analyzeText(String text, boolean isVowel) {
    //Treemap instead of Hash to keep the map sorted alphabetically so that it matches frontend output
    Map<String, Integer> textResult = new TreeMap<>();
    List<Character> vowels = Arrays.asList('a', 'e', 'i', 'o', 'u');

    ////Text is lower cased, filtered to be only letters
    String filteredText = text.toLowerCase().replaceAll("[^A-Za-z]", "");
    //Text is split into a character array
    char[] splitText = filteredText.toCharArray();



    for (char c : splitText) {
      if (isVowel) {
        //Vowels
        if (vowels.contains(c)) {
          textResult.put(String.valueOf(c), textResult.getOrDefault(String.valueOf(c), 0) + 1);
        }
      } else {
        //Consonants
        if (!vowels.contains(c)) {
          textResult.put(String.valueOf(c), textResult.getOrDefault(String.valueOf(c), 0) + 1);
        }
      }
    }
    return textResult;
  }
}
