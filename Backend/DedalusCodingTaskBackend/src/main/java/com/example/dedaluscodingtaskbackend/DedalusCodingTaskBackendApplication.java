package com.example.dedaluscodingtaskbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DedalusCodingTaskBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(DedalusCodingTaskBackendApplication.class, args);
	}

}
