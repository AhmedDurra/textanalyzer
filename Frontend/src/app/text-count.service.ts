import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, Observable, throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TextCountService {

  private apiUrl = 'http://localhost:8080/api/text/analyze';

  constructor(private http: HttpClient) {
  }

  // Sends a post request to the backend to analyze the text
  getTextResult(text: string, isVowel: boolean): Observable<any> {
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    const body = JSON.stringify({text: text, isVowel: isVowel});
    return this.http.post<any>(this.apiUrl, body, {headers}).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: any) {
    return throwError(error);
  }
}
