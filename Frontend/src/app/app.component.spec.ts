import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {TextCountService} from './text-count.service';
import {ToggleSwitchComponent} from "./toggle-switch/toggle-switch.component";
import {BrowserModule} from "@angular/platform-browser";
import {HttpClientModule} from "@angular/common/http";
import {RouterOutlet} from "@angular/router";
import {NgOptimizedImage} from "@angular/common";
import {AppRoutingModule} from "./app-routing.module";
import {of} from "rxjs";

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let textService: TextCountService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppComponent, ToggleSwitchComponent],
      imports: [BrowserModule,
        HttpClientModule,
        FormsModule,
        RouterOutlet,
        NgOptimizedImage,
        AppRoutingModule],
      providers: [TextCountService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    textService = TestBed.inject(TextCountService);
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize with default values', () => {
    expect(component.inputText).toEqual('');
    expect(component.isVowel).toBeFalse();
    expect(component.isOffline).toBeFalse();
    expect(component.allTextResults.length).toBe(0);
    expect(component.textResultEntry.size).toBe(0);
  });

  it('should analyze word offline for vowels', () => {
    component.isOffline = true;
    component.isVowel = true;
    component.inputText = 'Hi how are you doing  @ Elen *23 (See)';
    component.analyzeWord();

    expect(component.textResultEntry.size).toBe(5);
    expect(component.textResultEntry.get('a')).toBe(1);
    expect(component.textResultEntry.get('e')).toBe(5);
    expect(component.textResultEntry.get('i')).toBe(2);
    expect(component.textResultEntry.get('o')).toBe(3);
    expect(component.textResultEntry.get('u')).toBe(1);
    expect(component.textResultEntry.get('s')).toBeUndefined();
    expect(component.allTextResults.length).toBe(1);
    expect(component.allTextResults[0].text).toBe('Hi how are you doing  @ Elen *23 (See)');
  });

  it('should analyze word offline for consonants', () => {
    component.isOffline = true;
    component.isVowel = false;
    component.inputText = 'Hi how are you doing  @ Elen *23 (See)';
    component.analyzeWord();

    expect(component.textResultEntry.size).toBe(9);
    expect(component.textResultEntry.get('d')).toBe(1);
    expect(component.textResultEntry.get('g')).toBe(1);
    expect(component.textResultEntry.get('h')).toBe(2);
    expect(component.textResultEntry.get('l')).toBe(1);
    expect(component.textResultEntry.get('n')).toBe(2);
    expect(component.textResultEntry.get('r')).toBe(1);
    expect(component.textResultEntry.get('s')).toBe(1);
    expect(component.textResultEntry.get('w')).toBe(1);
    expect(component.textResultEntry.get('y')).toBe(1);
    expect(component.textResultEntry.get('o')).toBeUndefined();
    expect(component.allTextResults.length).toBe(1);
    expect(component.allTextResults[0].text).toBe('Hi how are you doing  @ Elen *23 (See)');
  });

  it('should analyze word online', () => {
    spyOn(textService, 'getTextResult').and.returnValue(of({ d: 1, g: 1, h: 2, l: 1, n: 2, r: 1, s: 1, w: 1, y: 1}));
    component.isOffline = false;
    component.inputText = 'Hi how are you doing  @ Elen *23 (See)';
    component.analyzeWord();

    expect(textService.getTextResult).toHaveBeenCalledWith('Hi how are you doing  @ Elen *23 (See)', component.isVowel);
    expect(component.allTextResults[0].result.get('d')).toBe(1);
    expect(component.allTextResults[0].result.get('g')).toBe(1);
    expect(component.allTextResults[0].result.get('h')).toBe(2);
    expect(component.allTextResults[0].result.get('l')).toBe(1);
    expect(component.allTextResults[0].result.get('n')).toBe(2);
    expect(component.allTextResults[0].result.get('r')).toBe(1);
    expect(component.allTextResults[0].result.get('s')).toBe(1);
    expect(component.allTextResults[0].result.get('w')).toBe(1);
    expect(component.allTextResults[0].result.get('y')).toBe(1);
    expect(component.allTextResults[0].result.get('o')).toBeUndefined();
    expect(component.allTextResults.length).toBe(1);
    expect(component.allTextResults[0].text).toBe('Hi how are you doing  @ Elen *23 (See)');
  });

  it('should increment the value of a letter in a map correctly', () => {
    component.incrementLetterValueInMap('a');
    component.incrementLetterValueInMap('b');
    component.incrementLetterValueInMap('a');

    expect(component.textResultEntry.get('a')).toBe(2);
    expect(component.textResultEntry.get('b')).toBe(1);
  });

  it('should clone map correctly', () => {
    const originalMap = new Map([['a', 1], ['e', 1]]);
    const clonedMap = component.cloneMap(originalMap);
    expect(clonedMap.size).toBe(2);
    expect(clonedMap.get('a')).toBe(1);
    expect(clonedMap.get('e')).toBe(1);
  });

  it('should convert object to map correctly', () => {
    const obj = {'a': 1, 'e': 1};
    const map = component.convertObjectToMap(obj);
    expect(map.size).toBe(2);
    expect(map.get('a')).toBe(1);
    expect(map.get('e')).toBe(1);
  });

  it('should toggle isVowel correctly', () => {
    component.toggleVowel(true);
    expect(component.isVowel).toBeTrue();

    component.toggleVowel(false);
    expect(component.isVowel).toBeFalse();
  });

  it('should toggle isOffline correctly', () => {
    component.toggleOffline(true);
    expect(component.isOffline).toBeTrue();

    component.toggleOffline(false);
    expect(component.isOffline).toBeFalse();
  });

  it('should update UI on input change', () => {
    const textarea = fixture.nativeElement.querySelector('textarea');
    textarea.value = 'Hello';
    textarea.dispatchEvent(new Event('input'));
    fixture.detectChanges();

    expect(component.inputText).toBe('Hello');
  });
});
