import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-toggle-switch',
  templateUrl: './toggle-switch.component.html',
  styleUrl: './toggle-switch.component.css'
})
export class ToggleSwitchComponent {

  //The choice that will emit true
  @Input() trueChoice: string = ''
  //The choice that will emit false
  @Input() falseChoice: string = ''
  @Output() selectedChoice = new EventEmitter<boolean>();

  //Unique name generated for the radio button group
  uniqueRadioName = this.trueChoice + this.falseChoice;

  isFirstChoiceSelected: boolean = false;

  choiceChanged(value: boolean) {
    this.isFirstChoiceSelected = value;
    this.selectedChoice.emit(this.isFirstChoiceSelected);
  }
}
