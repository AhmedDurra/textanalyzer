import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ToggleSwitchComponent } from './toggle-switch.component';
import { By } from '@angular/platform-browser';

describe('ToggleSwitchComponent', () => {
  let component: ToggleSwitchComponent;
  let fixture: ComponentFixture<ToggleSwitchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToggleSwitchComponent ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleSwitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have default values for inputs', () => {
    expect(component.trueChoice).toBe('');
    expect(component.falseChoice).toBe('');
  });

  it('should have initial state with isFirstChoiceSelected as false', () => {
    expect(component.isFirstChoiceSelected).toBeFalse();
  });

  it('should update isFirstChoiceSelected and emit selectedChoice on choiceChanged', () => {
    spyOn(component.selectedChoice, 'emit');

    component.choiceChanged(true);
    expect(component.isFirstChoiceSelected).toBeTrue();
    expect(component.selectedChoice.emit).toHaveBeenCalledWith(true);

    component.choiceChanged(false);
    expect(component.isFirstChoiceSelected).toBeFalse();
    expect(component.selectedChoice.emit).toHaveBeenCalledWith(false);
  });

  it('should render trueChoice and falseChoice labels correctly', () => {
    component.trueChoice = 'Yes';
    component.falseChoice = 'No';
    fixture.detectChanges();

    const labels = fixture.debugElement.queryAll(By.css('label span'));
    expect(labels[0].nativeElement.textContent.trim()).toBe('Yes');
    expect(labels[1].nativeElement.textContent.trim()).toBe('No');
  });

  it('should update isFirstChoiceSelected when radio button is clicked', () => {
    const radioButtons = fixture.debugElement.queryAll(By.css('input[type="radio"]'));

    radioButtons[0].nativeElement.click();
    fixture.detectChanges();
    expect(component.isFirstChoiceSelected).toBeTrue();

    radioButtons[1].nativeElement.click();
    fixture.detectChanges();
    expect(component.isFirstChoiceSelected).toBeFalse();
  });

  it('should set uniqueRadioName correctly based on trueChoice and falseChoice', () => {
    component.trueChoice = 'Yes';
    component.falseChoice = 'No';
    component.uniqueRadioName = component.trueChoice + component.falseChoice;
    expect(component.uniqueRadioName).toBe('YesNo');
  });
});
