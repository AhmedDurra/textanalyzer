import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TextCountService} from './text-count.service';

describe('TextCountService', () => {
  let service: TextCountService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TextCountService]
    });
    service = TestBed.inject(TextCountService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should send correct request for counting vowels', () => {
    const mockText = 'Hello';
    const isVowel = true;
    const mockResponse: object = {
      "e": 1,
    };

    service.getTextResult(mockText, isVowel).subscribe(response => {
      expect(response.e).toEqual(1);
    });

    const req = httpTestingController.expectOne('http://localhost:8080/api/text/analyze');
    expect(req.request.method).toEqual('POST');
    expect(req.request.body).toEqual(JSON.stringify({text: mockText, isVowel: true}));

    req.flush(mockResponse);
  });

  it('should send correct request for counting consonants', () => {
    const mockText = 'Hello';
    const isVowel = false;
    const mockResponse: object = {
      "l": 2,
    };

    service.getTextResult(mockText, isVowel).subscribe(response => {
      expect(response.l).toEqual(2);
    });

    const req = httpTestingController.expectOne('http://localhost:8080/api/text/analyze');
    expect(req.request.method).toEqual('POST');
    expect(req.request.body).toEqual(JSON.stringify({text: mockText, isVowel: false}));

    req.flush(mockResponse);
  });
});
