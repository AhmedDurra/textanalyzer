import {Component} from '@angular/core';
import {TextCountService} from "./text-count.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  vowels = ['a', 'e', 'i', 'o', 'u'];
  isVowel: boolean = false;
  isOffline: boolean = false;
  errorMessage: string = '';

  //A single entry of the analyzed text
  textResultEntry = new Map<string, number>();
  //Contains the record of all the analyzed text
  allTextResults: { text: string, result: Map<string, number> }[] = [];

  inputText = '';

  constructor(private textService: TextCountService) {
  }

  analyzeWord() {
    this.textResultEntry.clear();
    this.errorMessage = '';
    //Text is lower cased, filtered to be only letters, split into a character array and sorted alphabetically
    let filteredText = this.inputText.toLowerCase().replace(/[^A-Za-z]/g, '').split('').sort();
    if (this.isOffline) {
      for (let i = 0; i < filteredText.length; i++) {
        if (this.isVowel) {
          //Vowels
          if (this.vowels.includes(filteredText[i])) {
            this.incrementLetterValueInMap(filteredText[i]);
          }
        } else {
          //Consonant
          if (!this.vowels.includes(filteredText[i])) {
            this.incrementLetterValueInMap(filteredText[i]);
          }
        }
      }
      this.allTextResults.push({text: this.inputText, result: this.cloneMap(this.textResultEntry)});
    } else {
      this.textService.getTextResult(this.inputText, this.isVowel).subscribe((response) => {
          this.allTextResults.push({text: this.inputText, result: this.convertObjectToMap(response)})
        }, (error) => {
          this.errorMessage = "API Communication error, please switch to offline mode"
          console.log(error)
        }
      );
    }
  }

  //Sets the letter in a map and increments its value if found in the text
  incrementLetterValueInMap (letter: string) {
    this.textResultEntry.set(letter, (this.textResultEntry.get(letter) ?? 0) + 1);
  }

  cloneMap(originalMap: Map<any, any>): Map<any, any> {
    let newMap = new Map();
    originalMap.forEach((value, key) => {
      newMap.set(key, value);
    });
    return newMap;
  }

  convertObjectToMap(obj: any): Map<string, any> {
    const map = new Map<string, any>();
    for (const [key, value] of Object.entries(obj)) {
      map.set(key, value);
    }
    return map;
  }

  toggleVowel($event: boolean) {
    this.isVowel = $event;
  }

  toggleOffline($event: boolean) {
    this.isOffline = $event;
  }
}
